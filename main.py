import sqlite3
from sqlite3
import Error
import RPi.GPIO as GPIO
import sys
sys.path.append('/home/pi/MFRC522-python')
from mfrc522 
import SimpleMFRC522
from __future__ 
import print_function
import time

conn = sqlite3.connect('raspy.db') #conexion entre la base de datos 
reader = SimpleMFRC522()

print("Mantenga una identificacion cerca del lector") #busqueda de clave rfid en la base de datos

#----------------------------busqueda de id en la base de datos
try:
	id, text =reader.read()
	try:
		c=conn.cursor()
		c.execute("SELECT * FROM persona WHERE id=?",(id,))
		registro=c.fetchall()
		for row in registro:
			hmesa = (row[2]) #se guarda valor de altura de mesa		
			hsilla = (row[3]) #se guarda valor altura silla
	except Error as e :
		print ("Error while connecting to SQLITE", e)
finally:
	GPIO.cleanup()


#-------------------configuracion de GPIO para electrovalvulas------------------------------------ 
GPIO.setmode(GPIO.BOARD)
GPIO.setup(5, GPIO.OUT) #subir banquito
GPIO.setup(6, GPIO.OUT) #bajar banquito
GPIO.setup(13, GPIO.OUT) #subir mesa
GPIO.setup(19, GPIO.OUT) #bajar mesa

#-------------------definimos los GPIO para controlar los sensores ultrasonicos--------------------
GPIO.setmode(GPIO.BCM)# Use BCM GPIO references, instead of physical pin numbers
GPIO_TRIGGER = 18
GPIO_ECHO    = 24
GPIO_TRIGGERmesa = 17
GPIO_ECHOmesa    = 27

GPIO.setup(GPIO_TRIGGER,GPIO.OUT)  # Trigger
GPIO.setup(GPIO_ECHO,GPIO.IN)      # Echo
GPIO.setup(GPIO_TRIGGERmesa,GPIO.OUT)  # Trigger mesa
GPIO.setup(GPIO_ECHOmesa,GPIO.IN)      # Echo mesa

speedSound = 33100 #Una velocidad cualquiera





#-----------------------------------------------------FUNCIONES-------------------------------------------

def calcularDistanciaSilla():
	GPIO.output(GPIO_TRIGGER, False) #Set trigger to False (Low)
	time.sleep(0.5)# Allow module to settle

	GPIO.output(GPIO_TRIGGER, True)# Send pulse to trigger
	time.sleep(0.00001)	# Wait 

	GPIO.output(GPIO_TRIGGER, False)
	start = time.time()

	while GPIO.input(GPIO_ECHO)==0:
		start = time.time()
	while GPIO.input(GPIO_ECHO)==1:
		stop = time.time()

	elapsed = stop-start #Calculate pulse length, Distance pulse travelled in that time is time
	distance = elapsed * speedSound #multiplied by the speed of sound (cm/s)
	distance = int(distance / 2) #That was the distance there and back so halve the value
	return distance

def calcularDistanciaMesa():
	GPIO.output(GPIO_TRIGGERmesa, False)
	time.sleep(0.5)
	GPIO.output(GPIO_TRIGGERmesa, True)
	time.sleep(0.00001)
	GPIO.output(GPIO_TRIGGERmesa, False)
	start = time.time()
	while GPIO.input(GPIO_ECHOmesa)==0:
		start = time.time()
	while GPIO.input(GPIO_ECHOmesa)==1:
		stop = time.time()
	elapsed = stop-start
	distance = elapsed * speedSound
	distance = int(distance / 2)
	return distance




#-----------------------------------------------------MAIN--------------------------------
#COLOCAMOS EN EL OFFSET A LA SILLA
distancia2 = calcularDistanciaSilla()
while  distacia2 > 7:#while para reiniciar el banquito, bajarlo
	GPIO.output(5, False)
	GPIO.output(6, True)
	distancia2 = calcularDistanciaSilla()

#COLOCAMOS EN EL OFFSET A LA MESA
distancia3 = calcularDistanciaMesa()
while  distancia3 > alturaMesa: #while para reiniciar la mesa, bajarla
	GPIO.output(13, False)	
	GPIO.output(19, True)
	distancia3 = calcularDistanciaMesa()


alturaSilla=hsilla-34 #se le resta 34 porque es la constante de altura de la estructura
alturaMesa=hmesa-64 #se le resta 64 porque es la constante de altura de la estructura

distancia1 = calcularDistanciaSilla()
while  distacia1 != alturaSilla: #while para ajustar el banquito
	if  distancia1<alturaSilla:
		GPIO.output(5, True)
		GPIO.output(6, False)
	if  distancia1>alturaSilla:
		GPIO.output(5, False)
		GPIO.output(6, True)
	distancia1 = calcularDistanciaSilla()

distancia = calcularDistanciaMesa()
while  distancia != alturaMesa: #while para ajustar la mesa
	if  distancia<alturaMesa:
		GPIO.output(13, True)
		GPIO.output(19, False)
	if  distancia>alturaMesa:
		GPIO.output(13, False)	
		GPIO.output(19, True)
	distancia = calcularDistanciaMesa()

GPIO.cleanup() #Reset GPIO settings